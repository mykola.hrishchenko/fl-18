/* START TASK 1: Your code goes here */
const types = {
    DEFAULT: 'default',
    SPECIAL: 'special'
}

const colors = {
    WHITE: 'white',
    YELLOW: 'yellow',
    BLUE: 'blue',
    GREEN: 'green'
};

let cells = Array(3).fill(Array(3).fill({
    color: colors.WHITE,
    type: types.DEFAULT
}));
cells[1][2] = {color: colors.WHITE, type: types.SPECIAL };

function updateCells(clickedCell) {
    const { rowIndex, columnIndex } = clickedCell;
    let cellInfo = cells[rowIndex][columnIndex];

    if (columnIndex === 0) {
        cells[0] = cells[0].map((element) => {
            if (element.color !== colors.YELLOW) {
                element.color = colors.BLUE;
            }
            return element;
        });
    } else {
        if (cellInfo.type === types.SPECIAL) {
            cells = cells.map((row) => {
                return row.map((element) => {
                    if (element.color === colors.WHITE) {
                        element.color = colors.YELLOW;
                    }

                    return element;
                });
            });
        } else {
            cellInfo.color = colors.YELLOW; // getNextColor(cellInfo.color);
        }
    }

    // draw(cells);
}

// function draw() {
//     //changeColor('yellow', 0, 0);
//     // el.setAttribute('style', 'background-color: yellow');
// }
//
// function changeColor(event, rowIndex, cellIndex) {
//
// }

function onClick(e) {
    const rowIndex = e.target.parentElement.rowIndex;
    const columnIndex = e.target.cellIndex;

    updateCells({ rowIndex, columnIndex });
}
let el = document.getElementById('table')
el.addEventListener('click', onClick, false)

/* END TASK 1 */

/* START TASK 2: Your code goes here */

/* END TASK 2 */

/* START TASK 3: Your code goes here */

/* END TASK 3 */

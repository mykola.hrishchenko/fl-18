const appRoot = document.getElementById('app-root');

const header = document.createElement('header')

const text = document.createElement('h1')
text.innerText = 'Countries Search'

const typeOfSearch = document.createElement('form')

let checkChoice = ''

const chooseType = document.createElement('label')
const chooseSearch = document.createElement('label')

const firstChoice = document.createElement('div')
const secondChoice = document.createElement('div')

const choice1 = document.createElement('input')
const choice2 = document.createElement('input')
const label1 = document.createElement('label')
const label2 = document.createElement('label')

const list = document.createElement('select')
const firstOption = document.createElement('option')

const tbl = document.createElement('table')
const tblBody = document.createElement('tbody')
const tblWidth = 6
const tblHeader = ['Country name', 'Capital', 'World Region', 'Languages', 'Area', 'Flag']
const tblValues = ['name', 'capital', 'region', 'languages', 'area', 'flagURL']

chooseType.innerText = 'Please choose type of search:'
chooseSearch.innerText = 'Please choose search query:'

firstChoice.setAttribute('class', 'first-choice')
secondChoice.setAttribute('class', 'second-choice')

choice1.type = 'radio'
choice1.name = 'choose'
choice1.id = 'choice1'
choice1.value = 'by region'
choice1.setAttribute('onClick', 'checker(this)')

choice2.type = 'radio'
choice2.name = 'choose'
choice2.id = 'choice2'
choice2.value = "by language"
choice2.setAttribute('onClick', 'checker(this)')

label1.innerText = 'By Region'
label1.setAttribute('for', 'choice1')
label2.innerText = 'By Language'
label2.setAttribute('for', 'choice2')

firstOption.value = '';
firstOption.text = 'firstly put button';


appRoot.appendChild(header)
header.appendChild(text)
header.appendChild(typeOfSearch)

typeOfSearch.appendChild(firstChoice)
firstChoice.appendChild(chooseType)
firstChoice.appendChild(choice1)
firstChoice.appendChild(label1)
firstChoice.appendChild(choice2)
firstChoice.appendChild(label2)

typeOfSearch.appendChild(secondChoice)
secondChoice.appendChild(chooseSearch)
list.setAttribute('onChange', 'createTable(value)')
secondChoice.appendChild(list)
list.appendChild(firstOption)

const regionsList = externalService.getRegionsList()
const languagesList = externalService.getLanguagesList()

function checker(node) {
    checkChoice = node.value
    console.log(checkChoice)
    while (list.firstChild) {
        list.removeChild(list.lastChild);
    }
    if(node.value === 'by region') {
        for (let i = 0; i <= regionsList.length; i++) {
            let option = document.createElement('option')
            option.value = regionsList[i];
            option.text = regionsList[i];
            list.appendChild(option)
        }
    }
    if (node.value === 'by language') {
        for (let i = 0; i < languagesList.length; i++) {
            let option = document.createElement('option')
            option.value = languagesList[i];
            option.text = languagesList[i];
            list.appendChild(option)
        }
    }
}

function addTableHead() {
    let row = document.createElement('tr')
    for (let i = 0; i < tblWidth; i++) {
        let headCell = document.createElement('th')
        let cellText = document.createTextNode(tblHeader[i])
        headCell.appendChild(cellText)
        row.appendChild(headCell)
    }
    tblBody.appendChild(row)
}

function createTable(value) {
    let currentElement
    if (checkChoice === 'by region') {
        currentElement = externalService.getCountryListByRegion(value)
    } else {
        currentElement = externalService.getCountryListByLanguage(value)
    }
    console.log(currentElement)
    while (tblBody.firstChild) {
        tblBody.removeChild(tblBody.lastChild);
    }
    console.log(value)
    addTableHead()
    for (let i = 0; i < currentElement.length; i++) {
        let row = document.createElement('tr')
        for (let j = 0; j < tblWidth; j++) {
            let cell = document.createElement('td')
            if (i === 0 || i % 2 === 0) {
                cell.setAttribute('class', 'gray')
            } else {
                cell.setAttribute('class', 'white')
            }
            if (tblValues[j] === 'languages') {
                let cellText = document.createTextNode(Object.values(currentElement[i][tblValues[j]]).toString())
                cell.appendChild(cellText)
                row.appendChild(cell)
            }else if (tblValues[j] === 'flagURL') {
                let img = document.createElement('img')
                img.setAttribute('src', 'https://icons.iconarchive.com/icons/blackvariant/button-ui-app-pack-one/64/Lite-Icon-icon.png')
                cell.appendChild(img)
                row.appendChild(cell)
            } else {
                let cellText = document.createTextNode(currentElement[i][tblValues[j]])
                cell.appendChild(cellText)
                row.appendChild(cell)
            }
        }
        tblBody.appendChild(row)
    }
    tbl.appendChild(tblBody)
    appRoot.appendChild(tbl)
    tbl.setAttribute("border", "2");
}

//list of all regions
externalService.getRegionsList()
//list of all languages
externalService.getLanguagesList()
//get countries list by language
externalService.getCountryListByLanguage()
//get countries list by region
externalService.getCountryListByRegion()


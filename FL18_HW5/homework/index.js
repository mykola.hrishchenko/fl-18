function isEquals(first, second) {
    return first === second;
}
//console.log(isEquals(3, 3))

function isBigger(first, second) {
    return first > second
}
//console.log(isBigger(5, -1))

function storeNames() {
    let result = []
    for (let i = 0; i < arguments.length; i++) {
        result.push(arguments[i])
    }
    return result
}
//console.log(storeNames('Tommy Shelby', 'Ragnar Lodbrok', 'Tom Hardy'))

function getDifference(first, second) {
        if (first - second < 0) {
            return (first - second) * -1
        }else {
            return first - second
        }
    }
//console.log(getDifference(8, 5))

function negativeCount(arr) {
    let counter = 0
    for (let i = 0; i < arr.length; i++) {
        if (arr[i] < 0) {
            counter++
        }
    }
    return counter
}
//console.log(negativeCount([0, -3, 5, 7, -2]))

function letterCount(word, letter) {
    let counter = 0
    for (let i = 0; i < word.length; i++) {
        if (word.charAt(i) === letter) {
            counter++
        }
    }
    return counter
}
//console.log(letterCount("Marry", "r"))

function countPoints(array) {
    let counter = 0;
    for (let i = 0; i < array.length; i++) {
        let arrayOfStrings = array[i].split(':')
        if (Number(arrayOfStrings[0]) > Number(arrayOfStrings[1])) {
            counter += 3
        }else if (Number(arrayOfStrings[0]) === Number(arrayOfStrings[1])) {
            counter += 1
        }
    }
    return counter
}
//console.log(countPoints(['100:90', '110:98', '100:100', '95:46', '54:90', '99:44', '90:90', '111:100']))
// function isFunction(functionToCheck) {
// 	return functionToCheck && {}.toString.call(functionToCheck) === '[object Function]';
// }

const pipe = (value, ...funcs) => {
	try {
		for (let i = 0; i < funcs.length; i++) {
			value = funcs[i](value)
		}
	} catch (e) {
		value = 'Provided argument at position ... is not a function!'
	}
	return value
};

const replaceUnderscoreWithSpace = (value) => value.replace(/_/g, ' ');
const capitalize = (value) =>
	value
		.split(' ')
		.map((val) => val.charAt(0).toUpperCase() + val.slice(1))
		.join(' ');
 const appendGreeting = (value) => `Hello, ${value}!`;


// const error = pipe('john_doe', replaceUnderscoreWithSpace, capitalize, '');
// console.log(error)
// // alert(error); // Provided argument at position 2 is not a function!

// const result = pipe('john_doe', replaceUnderscoreWithSpace, capitalize, appendGreeting);
// console.log(result)
// //
// // alert(result); // Hello, John Doe!

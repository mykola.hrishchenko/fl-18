function visitLink(path) {
    if (path === 'Page1') {
        if (localStorage.Page1) {
            localStorage.Page1 = Number(localStorage.Page1) + 1;
        } else {
            localStorage.Page1 = 0;
        }
    } else if (path === 'Page2') {
        if (localStorage.Page2) {
            localStorage.Page2 = Number(localStorage.Page2) + 1;
        } else {
            localStorage.Page2 = 0;
        }
    } else if (path === 'Page3') {
        if (localStorage.Page3) {
            localStorage.Page3 = Number(localStorage.Page3) + 1;
        } else {
            localStorage.Page3 = 0;
        }
    }
}

function viewResults() {
    let div = document.querySelector('.container')
    let ul = document.createElement('ul')

    div.appendChild(ul)

    let clicks = [localStorage.getItem('Page1'),localStorage.getItem('Page2'), localStorage.getItem('Page3')]

    for (let i = 0; i < clicks.length; i++) {
        let li = document.createElement('li')
        li.innerHTML = clicks[i]
        ul.appendChild(li)
    }
}

let data = [
    {
        '_id': '5b5e3168c6bf40f2c1235cd6',
        'index': 0,
        'age': 39,
        'eyeColor': 'green',
        'name': 'Stein',
        'favoriteFruit': 'apple'
    },
    {
        '_id': '5b5e3168e328c0d72e4f27d8',
        'index': 1,
        'age': 38,
        'eyeColor': 'blue',
        'name': 'Cortez',
        'favoriteFruit': 'strawberry'
    },
    {
        '_id': '5b5e3168cc79132b631c666a',
        'index': 2,
        'age': 2,
        'eyeColor': 'blue',
        'name': 'Suzette',
        'favoriteFruit': 'apple'
    },
    {
        '_id': '5b5e31682093adcc6cd0dde5',
        'index': 3,
        'age': 17,
        'eyeColor': 'green',
        'name': 'Weiss',
        'favoriteFruit': 'banana'
    }
]
function reverseNumber(num) {
    return parseFloat(num.toString().split('').reverse().join('')) * Math.sign(num)

}
function forEach(arr, func) {
    let array = []
    for (let i = 0; i < arr.length; i++) {
        array.push(func(arr[i]))
    }
    return array
}

//forEach([2, 5, 8], function(el) { console.log(el) })
function map(arr, func) {
    return forEach(arr, func)
}

//console.log(map([1, 2, 3, 4],  function(el) { return el + 3; }))
//console.log(map([1, 2, 3, 4, 5], function (el) { return el * 2; }))
function filter(arr, func) {
    let filteredArr = []

    let array = forEach(arr, func)
    for(let i = 0; i < array.length; i++) {
        if(array[i]) {
            filteredArr.push(arr[i])
        }
    }
    return filteredArr
}

//console.log(filter([2, 5, 1, 3, 8, 6], function(el) { return el > 3 }))
//console.log(filter([1, 4, 6, 7, 8, 10], function(el) {  return el % 2 === 0 }))

function getAdultAppleLovers(data) {
    let result = []
    for (let i = 0; i < data.length; i++) {
        for (const property in data[i]) {
            if (Number(data[i]['age']) > 18 && property === 'favoriteFruit' && data[i][property] === 'apple') {
                result.push(data[i]['name'])
            }
        }
    }
    return result
}
//console.log(getAdultAppleLovers(data))

function getKeys(obj) {
    let keysArray = []
    for (const property in obj) {
        keysArray.push(property)
    }
    return keysArray
}
//console.log(getKeys({keyOne: 1, keyTwo: 2, keyThree: 3}))

function getValues(obj) {
    let valuesArray = []
    for (const property in obj) {
        valuesArray.push(obj[property])
    }
    return valuesArray
}
//console.log(getValues({keyOne: 1, keyTwo: 2, keyThree: 3}))

function showFormattedDate(dateObj) {
    const options = {month: 'long'};
    let month = dateObj.toLocaleDateString('en-US', options).substring(0, 3)
    let date = dateObj.getDate()
    let year = dateObj.getFullYear()

    return 'It is ' + date + ' of ' + month + ', ' + year
}
//console.log(showFormattedDate(new Date('2018-08-27T01:10:00')))
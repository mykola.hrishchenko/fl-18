function getAge(birthday) {
    const date = new Date();
    const today = new Date(date.getFullYear(), date.getMonth(), date.getDate());
    const birthThisYear = new Date(today.getFullYear(), birthday.getMonth(), birthday.getDate())

    let age = today.getFullYear() - birthday.getFullYear();
    if (today < birthThisYear) {
        age--
    }

    return age
}
// const  birthday = new Date(2000, 10, 18);
// const age = getAge(birthday)
// console.log(age)

function getWeekDay(date) {
    let options = { weekday: 'long'};
    return new Intl.DateTimeFormat('en-US', options).format(date)
}
// console.log(getWeekDay(Date.now()));
// console.log(getWeekDay(new Date(2020, 9, 22)));

function getAmountDaysToNewYear() {
    let date = new Date()
    let newYear = new Date(date.getFullYear()+1, 0, 1, 0, 0, 0)

    let millisToNY = newYear - date

    millisToNY = Math.round(millisToNY / 1000 / 60 / 60 / 24)

    return millisToNY
}
//console.log(getAmountDaysToNewYear())

function howFarIs(weekdayToCheck) {
    weekdayToCheck = weekdayToCheck.toLowerCase()
    const days = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday']
    let date = new Date()
    let numOfWeekday, daysToWeekday, result

    for (let i = 0; i < days.length; i++) {
        if (days[i] === weekdayToCheck) {
            numOfWeekday = i + 1
        }
    }

    let str = weekdayToCheck.charAt(0).toUpperCase() + weekdayToCheck.substring(1).toLowerCase()
    let options = { weekday: 'long'};
    let weekday = new Intl.DateTimeFormat('en-US', options).format(date)

    if (numOfWeekday === date.getDay()) {
        result = 'Hey, today is ' + weekday + ' =)'
    } else {
        if (numOfWeekday > date.getDay()) {
            daysToWeekday = numOfWeekday - date.getDay()
            result = 'It is ' + daysToWeekday + ' day(s) left till ' + str + '.'
        } else if (numOfWeekday < date.getDay()) {
            daysToWeekday = 7 - date.getDay() + numOfWeekday
            result = 'It is ' + daysToWeekday + ' day(s) left till ' + str + '.'
        }
    }

    return result
}
//console.log(howFarIs('monday'));

function isValidIdentifier (variable) {
    variable = variable.toLowerCase()
    const checker = 'abcdefghijklmnopqrstuvwxyz1234567890_$'
    const checkFirst = '1234567890'
    let result = true
    let counter = 0
    for (let i = 0; i < variable.length; i++) {
        for (let j = 0; j < checker.length; j++) {
            if (variable[i] !== checker[j]) {
                counter++
            }
        }
        if (counter === checker.length) {
            result = false;
        }
        counter = 0
    }
    for (let i = 0; i < checkFirst.length; i++) {
        if (variable.charAt(0) === checkFirst[i]) {
            result = false
        }
    }

    return result
}
//console.log(isValidIdentifier('myVar'));

function capitalize(testStr) {
    let arrayOfWords = testStr.split(' ')
    let result = ''

    for(let i = 0; i < arrayOfWords.length; i++) {
        if (i === arrayOfWords.length - 1) {
            result += arrayOfWords[i].charAt(0).toUpperCase() + arrayOfWords[i].slice(1)
        } else {
            result += arrayOfWords[i].charAt(0).toUpperCase() + arrayOfWords[i].slice(1) + ' '
        }
    }

    return result
}
// const testStr = "My name is John Smith. I am 27."
// console.log(capitalize(testStr))

function isValidAudioFile(file) {
    file = file.toLowerCase()
    const checker = 'abcdefghijklmnopqrstuvwxyz'
    const formats = ['mp3', 'flac', 'alac', 'aac']
    let array = file.split('.')
    let first = array[0]
    let second = array[1]
    let counter = 0
    let result = true

    for (let i = 0; i < first.length; i++) {
        for (let j = 0; j < checker.length; j++) {
            if (first.charAt(i) !== checker[j]) {
                counter++
            }
        }
        if (counter === checker.length) {
            result = false;
        }
        counter = 0
    }

    for (let i = 0; i < formats.length; i++) {
        if (formats[i] !== second) {
            counter++
        }
    }
    if (counter === formats.length) {
        result = false;
    }

    return result
}
//console.log(isValidAudioFile('file.mp3'));

function getHexadecimalColors(stringToCheck) {
    let arrayOfWords = stringToCheck.split(' ')
    let arrayOfColors = []
    let result = []

    for (let i = 0; i < arrayOfWords.length; i++) {
        if (arrayOfWords[i].charAt(0) === '#') {
            arrayOfWords[i] = arrayOfWords[i].replace(';', '')
            arrayOfColors.push(arrayOfWords[i])
        }
    }

    for (let i = 0; i < arrayOfColors.length; i++) {
        if (arrayOfColors[i].length === 4 || arrayOfColors[i].length === 7) {
            result.push(arrayOfColors[i])
        }
    }

    return result;
}
// const testString = "color: #3f3; background-color: #AA00ef; and: #abcd";
// console.log(getHexadecimalColors('red and #0000'));

function isValidPassword(password) {
    const lowerCase = 'abcdefghijklmnopqrstuvwxyz'
    const upperCase = lowerCase.toUpperCase()
    const numbers = '1234567890'
    let result = true

    let upperLetter = false, lowerLetter = false, number = false

    if (password.length >= 8) {
        for (let i = 0; i < password.length; i++) {
            for (let j = 0; j < lowerCase.length; j++) {
                if (lowerCase.charAt(j) === password.charAt(i)) {
                    lowerLetter = true
                }
            }
            for (let j = 0; j < upperCase.length; j++) {
                if (upperCase.charAt(j) === password.charAt(i)) {
                    upperLetter = true
                }
            }
            for (let j = 0; j < numbers.length; j++) {
                if (numbers.charAt(j) === password.charAt(i)) {
                    number = true
                }
            }
        }
        if (!number || !lowerLetter || !upperLetter) {
            result = false
        }
    } else {
        result = false
    }

    return result;
}
// console.log(isValidPassword('Agent007'));

function addThousandsSeparators(numberToString) {
    return numberToString.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
}
// console.log(addThousandsSeparators('1234567890'));

function getAllUrlsFromText(stringForCheck) {
    let array = stringForCheck.split(' ')
    let arrayURL = []

    for (let i = 0; i < array.length; i++) {
        if (array[i].charAt(array[i].length - 1) === '/') {
            arrayURL.push(array[i])
        }
    }

    return arrayURL
}
// const text1 = "We use https://translate.google.com/ to translate some words and phrases from https://angular.io/";
// console.log(getAllUrlsFromText(text1));
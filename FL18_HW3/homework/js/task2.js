let start = confirm('Do you want to play a game?')
if (!start) {
    alert('You did not become a billionaire, but can.')
} else {
    startGame()
}

function startGame () {
    let random, choice, continueGame;
    let play = true;
    let attempt = 3;
    let totalPrize = 0;
    let currentPrize = 100;
    let maxPrize = 100;
    let maxRandomNumber = 8;

    while (play) {
        choice = Number(prompt('Choose a roulette pocket number from 0 to ' + maxRandomNumber +
            '\nAttempts left: ' + attempt +
            '\nTotal prize: ' + totalPrize + '$' +
            '\nPossible prize on current attempt: ' + currentPrize +
            '$\n'))
        random = Math.floor(Math.random() * 9);
        console.log(choice)
        console.log(random)
        if (choice === random) {
            totalPrize = currentPrize;
            alert('Congratulation, you won! Your prize is: ' + totalPrize + '$')
            continueGame = confirm('Do you want to continue?')
            if (continueGame) {
                attempt = 3;
                maxPrize *= 2;
                currentPrize = maxPrize;
                maxRandomNumber += 4;
            } else {
                alert('Thank you for your participation. Your prize is: ' + totalPrize + '$');
                let restart = confirm('Do you want play again? ')
                if (restart) {
                    startGame();
                    play = false;
                } else {
                    break;
                }
            }
        } else {
            attempt -= 1;
            currentPrize /= 2;
            if (attempt === 0) {
                alert('Thank you for your participation. Your prize is: ' + totalPrize + '$');
                let restart = confirm('Do you want play again? ')
                if (restart) {
                    startGame();
                    play = false;
                } else {
                    play = false;
                }
            }
        }
    }
}
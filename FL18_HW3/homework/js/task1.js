function checkInput (num, checker) {
    if (typeof num !== 'number'){
        alert('Invalid input data!');
    }
    if (checker === 100) {
        if (num > checker) {
            alert('Invalid input data!');
        }
    } else if (num <= checker) {
        alert('Invalid input data!');
    }
}

let money = Number(prompt('Enter initial amount of money!'))
checkInput(money, 999);
let years = Number(prompt('Enter number of years!')) | 0;
checkInput(years, 0);
let percentage = Number(prompt('Enter percentage of a year!'))
checkInput(percentage, 100);

let partOfMoney, profit = 0, result = money;

if (years === 1) {
    partOfMoney = result / 100 * percentage;
    result = result + partOfMoney;
    profit = partOfMoney;
} else {
    for (let i = 0; i < years; i++) {
        partOfMoney = result / 100 * percentage;
        result += partOfMoney;
        profit += partOfMoney;
    }
}

alert('Initial amount: ' + money +
    '\nNumber of years: ' + years +
    '\nPercentage of year: ' + percentage +'' +
    '\n\nTotal profit: ' + profit.toFixed(2) +
    '\nTotal amount: ' + result.toFixed(2));
